class CreateLikeFroms < ActiveRecord::Migration[5.2]
  def change
    create_table :hospital_to_pharmacies do |t|
      t.references :pharmacy
      t.references :hospital

      t.timestamps
    end
  end
end
