class CreateLikeTos < ActiveRecord::Migration[5.2]
  def change
    create_table :pharmacy_to_hospitals do |t|
      t.references :pharmacy
      t.references :hospital

      t.timestamps
    end
  end
end
