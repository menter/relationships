# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name: 'Daisuke', email: 'iamworkingwell@gmail.com', password: 'asdqwe123', role: 'Admin', bio: 'I am an Admin', price: 100)

20.times do |number|
  User.create(name: "第#{number}病院", email: "#{number}@byouin.com", password: 'asdqwe123', role: 'Hospital',
              bio: "This is #{number} the best hospital", place: %w[茨城 東京 埼玉 千葉 神奈川].sample,
              price: rand(10000000..100000000))
end

20.times do |number|
  User.create(name: "第#{number}薬局", email: "#{number}@yakkyoku.com", password: 'asdqwe123', role: 'Pharmacy',
              bio: "This is #{number} the best Pharmacy", place: %w[茨城 東京 埼玉 千葉 神奈川].sample,
              price: rand(10000000..100000000))
end