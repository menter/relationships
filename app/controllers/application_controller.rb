class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include SessionsHelper

  private

  def require_admin_logged_in
    if !logged_in? || current_user.role != 'Admin'
      flash[:danger] = '権限がありません'
      redirect_to root_path
    end
  end
end
