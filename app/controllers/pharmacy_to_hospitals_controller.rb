class PharmacyToHospitalsController < ApplicationController
  def create
    @pharmacy = PharmacyToHospital.new(hospital_id: params[:id], pharmacy_id: session[:user_id])
    if @pharmacy.save
      redirect_to pharmacy_path, notice: 'Succeed'
    else
      flash[:error] = @pharmacy.errors.full_messages
      render :'pharmacies/show'
    end
  end
end
