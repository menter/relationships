class SessionsController < ApplicationController
  def new
  end

  def create
    #byebug
    email = params[:email]
    password = params[:password]
    if login(email, password)
      flash[:success] = "#{session[:user_name]}様のログインに成功しました"
      case
      when session[:role] == 'Admin'
        redirect_to users_path
      when session[:role] == 'Pharmacy'
        redirect_to hospitals_path
      end
    else
      flash[:danger] = 'Emailかパスワードが間違っています'
      render 'sessions/new'
    end
  end

  def destroy
    session[:user_id] = nil
    session[:role] = nil
    session[:user_name] = nil
    flash[:success] = 'ログアウトしました'
    redirect_to root_url
  end

  private

  def login(email, password)
    #byebug
    @user = User.find_by(email: email)
    if @user&.authenticate(password)
      session[:user_id] = @user.id
      session[:user_name] = @user.name
      # if @user.role == 'Admin'
      #   session[:role] = 'Admin'
      # end
      case
      when @user.role == 'Admin' then
        session[:role] = 'Admin'
      when @user.role == 'Hospital' then
        session[:role] = 'Hospital'
      when @user.role == 'Pharmacy' then
        session[:role] = 'Pharmacy'
      end
      true
    else
      false
    end
  end
end