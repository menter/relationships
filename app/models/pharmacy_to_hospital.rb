class PharmacyToHospital < ApplicationRecord
  belongs_to :pharmacy, class_name: 'User'
  belongs_to :hospital, class_name: 'User'
end
