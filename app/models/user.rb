class User < ApplicationRecord
  before_save { self.email.downcase! }
  validates :role, presence: true
  validates :name, presence: true, length: {maximum: 50}
  validates :email, presence: true, length: {maximum: 255},
            format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i },
            uniqueness: { case_sensitive: false }
  validates :bio, presence: true, length: {maximum: 500}
  validates :password, presence: true
  validates :price, presence: true
  has_secure_password
  mount_uploader :image, ImageUploader

  has_many :hospital_to_pharmacies, foreign_key: 'hospital_id'
  has_many :pharmacy_to_hospitals, foreign_key: 'pharmacy_id'
end
