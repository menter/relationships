Rails.application.routes.draw do

  resources :hospitals, only: %i[index show]
  resources :pharmacies, only: [:index, :show]
  root to: 'toppages#index'
  resources :users
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  resources :pharmacy_to_hospitals, only: [:create]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
